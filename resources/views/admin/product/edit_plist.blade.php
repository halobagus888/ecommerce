@extends('admin/master')

@section('content')
@if ($errors->any())
	<div class="alert alert-danger">
		<ul>
		     @foreach ($errors->all() as $error)
		         <li>{{$error}}</li>
		     @endforeach
	    </ul>
    </div>
@endif

@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif


<form method="post" action="{{url('admin/product/edit/'.$dataprodukk->id)}}">
	@csrf

	<div class="card-body">
		<div class="form-group">
			<label for="categories">Kategori</label><br>
			<select class="form-control" name="category_id">
				<option value="{{$dataprodukk->category->id}}">{{$dataprodukk->category->nama}} - terpilih</option>
				@foreach ( $categories as $row )
					<option value="{{$row->id}}">{{$row->nama}}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group">
			<label for="code">Kode Barang</label>
			<input class="form-control" placeholder="Input Kode Barang (Number)" type="text" name="code" value="{{old('code',$dataprodukk->code)}}">
			@error('code')
	        	<div class="invalid-feedback">{{ $message }}</div>
	        @enderror
		</div>
		<div class="form-group">
			<label for="name">Nama</label>
			<input class="form-control" placeholder="Nama Barang" type="text" name="name" value="{{old('name',$dataprodukk->name)}}">
			@error('name')
	        	<div class="invalid-feedback">{{ $message }}</div>
	        @enderror
		</div>
		<div class="form-group">
			<label for="stock">Stock</label>
			<input class="form-control" placeholder="Input Stock Tersedia (Number)" type="text" name="stock" value="{{old('stock',$dataprodukk->stock)}}">
			@error('stock')
	        	<div class="invalid-feedback">{{ $message }}</div>
	        @enderror
		</div>
		<div class="form-group">
			<label for="varian">Varian</label>
			<input class="form-control" placeholder="Varian Barang" type="text" name="varian" value="{{old('varian',$dataprodukk->varian)}}">
			@error('varian')
	        	<div class="invalid-feedback">{{ $message }}</div>
	        @enderror
		</div><br>
		
	    <button type="submit" name="submit" class="btn btn-primary">Submit</button>
	</div>
<!-- /.card-body -->
</form>

@endsection