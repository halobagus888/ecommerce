@extends('admin/master')

@section('content')
@if ($errors->any())
	<div class="alert alert-danger">
		<ul>
		     @foreach ($errors->all() as $error)
		         <li>{{$error}}</li>
		     @endforeach
	    </ul>
    </div>
@endif

@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif


<form method="post" action="{{url('admin/order/{order_id}/insertitem')}}">
@csrf
@foreach ( $olist as $row=>$order )
<input type="hidden" name="order_id" value="{{$order->id}}" id="order_id">
@endforeach
<div class="card-body">
	<div class="form-group">
	<label>Nama Produk</label><br>
		<select class="form-control" name="product_id">
		@foreach ( $dataprodukk as $row )
			<option value="{{$row->id}}" {{ (old('row') == $row->name) ? 'selected' : ''}}>{{$row->name}} - {{$row->varian}}</option>
		@endforeach
		</select>
	</div>
<div class="form-group">
	<label>Jumlah</label>
		<input class="form-control" placeholder="Jumlah Item" type="text" name="qty" value="{{old('qty')}}">
</div><br>
                  
<button type="submit" name="submit" class="btn btn-primary">Submit</button>
</div>
</form>

@endsection