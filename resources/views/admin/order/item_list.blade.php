@extends('admin/master')

@section('content')
@if ($errors->any())
  <div class="alert alert-danger">
    <ul>
         @foreach ($errors->all() as $error)
             <li>{{$error}}</li>
         @endforeach
      </ul>
    </div>
@endif

@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
  <!DOCTYPE html>
  <html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Order Items</h3>
              </div>

              <form method="post" action="{{url('admin/order/{order_id}/insertitem')}}" id="form-order-item">
              @csrf
              
              <input type="hidden" name="order_id" value="{{$order_id}}" id="order_id">
              
              <div class="card-body">
                <div class="form-group">
                <label for="product_id">Nama Produk</label><br>
                  <select id="product_id" class="form-control" name="product_id">
                  @foreach ( $dataprodukk as $row )
                    <option value="{{$row->id}}" {{ (old('row') == $row->name) ? 'selected' : ''}}>{{$row->name}} - {{$row->varian}}</option>
                  @endforeach
                  </select>
                </div>
              <div class="form-group">
                <label>Jumlah</label>
                  <input class="form-control" placeholder="Jumlah Item" type="text" name="qty" value="{{old('qty')}}" id="qty">
              </div><br>
                                
              <button type="submit" name="submit" class="btn btn-primary">Submit</button>
              </div>
              </form>
              <!-- /.card-header -->
              <div class="card-body">
                <a style="margin-bottom: 15px;" href="{{url('admin/order')}}" class="btn btn-primary">Back</a>
                <!-- <a style="margin-bottom: 15px;" href="{{url('admin/order/{order_id}/insertitem')}}" class="btn btn-primary">Insert Order</a> -->
                <table id="example2" class="table table-bordered table-hover">
                  <tr> 
                    <th style="text-align: center;">PRODUK</th>
                    <th style="text-align: center;">VARIAN PRODUK</th>
                    <th style="text-align: center;">QTY</th>
                    <th style="text-align: center;">MENU</th>
                  </tr>
                  <tbody id="data-item">
                  @foreach($ilist as $row=>$item)
                    <tr>
                      <td><?php echo $item->product->name; ?></td>
                      <td><?php echo $item->product->varian; ?></td>
                      <td><?php echo $item->qty; ?></td>
                      <td style="text-align: center;">
                        <a class="btn btn-xs btn-primary" href="{{url('admin/order/'.$item->id.'/edititem')}}">Edit</a>
                        <a href="{{url('admin/order/'.$item->id.'/hapus')}}" class="btn btn-xs btn-danger btn-delete" onclick="return confirm('Yakin dihapus?');" >Delete</a>
                      </td>
                    </tr>
                  @endforeach
                  </tbody>
                </table>
                
                <style>
                  .w-5{display: none;}
                </style>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </body>
  </html>
@endsection

@push('scrips')
  <script>
      $(document).ready(function() {
        $('#form-order-item').on('submit', function(e) {
          e.preventDefault();
          const order_id = $('#order_id').val();
          const product_id = $('#product_id').val();
          const qty = $('#qty').val();

          $.ajax({
            type: 'POST',
            url: '/api/order/' + order_id,
            data: {
              'order_id': order_id,
              'product_id': product_id,
              'qty': qty
            },
            success: function(result){
              $('#data-item').html(updateTable(result.data));
            }
          })
        })
      })


      // $(document).on('click', function(e){
      //     if($(e.target).hasClass('btn-delete')) {
      //       const id = $(e.target).data('id');
      //       $.ajax({
      //         type: 'DELETE',
      //         url: `/api/order/${id}/hapus`,
              
      //         success: function(result){
      //           $('#data-item').html(updateTable(result.data));
      //         }
      //       })
      //     }
      // })
      

      function updateTable(data) {
        let table = '';
        data.forEach((d, i) => {
          table += `
                    <tr>
                      <td>${d.product.name}</td>
                      <td>${d.product.varian}</td>
                      <td>${d.qty}</td>
                      <td style="text-align: center;">
                        <a class="btn btn-xs btn-primary" href="#">Edit</a>
                        <button type="button" class="btn btn-xs btn-danger btn-delete" 
                        data-order-id="${d.order_id}"
                        data-id="${d.id}"
                        onclick="return confirm('Yakin dihapus?');" >Delete</a>
                      </td>
                    </tr>

          `;
        })
        return table
      }
  </script>
@endpush