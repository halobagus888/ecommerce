<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Category;
use App\Models\Product;
use App\Models\Order;
use App\Models\OrderItem;

class OrderItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($order_id)
    {
        $data = OrderItem::with('product')->where('order_id', $order_id)->get();

        return response()->json(['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'order_id' => 'required|integer|exists:orders,id',
            'product_id' => 'required|integer|exists:products,id',
            'qty' => 'required',
        ]);


        $data = $request->all();
        OrderItem::create($data);
        $orderitem = OrderItem::with('product')->where('order_id', $data['order_id'])->get();
        return response()->json(['data' => $orderitem]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($order_id, $id)
    {
        $data = OrderItem::with('product')->where('order_id', $order_id)->where('id', $id)->get();

        return response()->json(['data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $order_id, $id)
    {
        $request->validate([
            'order_id' => 'required|integer|exists:orders,id',
            'product_id' => 'required|integer|exists:products,id',
            'qty' => 'required',
        ]);

        $data = OrderItem::with('product')->where('order_id', $order_id)->where('id', $id)->update(
            [
                'order_id' => $request->order_id,
                'product_id' => $request->product_id,
                'qty' => $request->qty,
            ]
        );
        $orderitem = OrderItem::with('product')->where('id', $id)->get();
        return response()->json(['data' => $orderitem]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function search($product_id){
        $data = OrderItem::with('product')->where('product_id', $product_id)->get();

        return response()->json(['data' => $data]);
    }

    public function destroy($order_id, $id)
    {

        OrderItem::where('order_id', $order_id)->where('id', $id)->delete();
        $orderitem = OrderItem::with('product')->where('order_id', $order_id)->get();
        
        return response()->json(['data' => $orderitem]);
    }
}
