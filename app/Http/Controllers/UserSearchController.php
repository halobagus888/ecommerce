<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Category;
use App\Models\Product;
use App\Models\Order;
use App\Models\OrderItem;
use File;

class UserSearchController extends Controller
{

    public function index(Request $request){
        // dd(request('search'));

        $dataprodukk = Product::orderBy('id','DESC')->paginate(10);
        if (request('search')) {
            // code...
            $dataprodukk->where('varian', 'like', '%' . request('search') . '%');
        }

        return view('divisima.search', ['plists'=>$dataprodukk]);
    }
}
