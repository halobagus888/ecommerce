<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Category;
use App\Models\Product;
use App\Models\Order;
use App\Models\OrderItem;
use File;
use Product as GlobalProduct;

class UserController extends Controller
{
    //
    public function index()    {
        return view('divisima.index');
    }

    public function cart()
    {
        $ulists = User::get();
        $olist = Order::get();
        $ilist = OrderItem::get();
        $dataprodukk = Product::get();
        return view('divisima.cart', compact('ulists', 'olist', 'ilist', 'dataprodukk'));
        
    }

    public function category(Request $request)
    {

        $search = $request->input('search');;

        $dataprodukk = Product::query()
            ->where('varian', 'LIKE', "%{$search}%")
            ->get();
        

        return view('divisima.category', ['plists'=>$dataprodukk]);
    }

    public function checkout()
    {
        return view('divisima.checkout');
    }

    public function contact()
    {
        return view('divisima.contact');
    }

    public function product()
    {
        return view('divisima.product');
    }
}
